<?php
namespace App\Email;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;

class Email extends DB{
    public $id = "";
    public $name = "";
    public $email = "";
    public function __construct()
    {
        parent::__construct();
        echo "<br>";
    }
    public function setData($data=NULL){
        if(array_key_exists("id",$data)){
            $this->id=$data["id"];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data["name"];
        }
        if(array_key_exists("email",$data)){
            $this->email=$data["email"];
        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->name,$this->email);
        $query="insert into email(name,email) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $result= $sth->execute($values);

        if($result){
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Email: $this->emali ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }else{
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Email: $this->email ] <br> Data Has Not Been Inserted Successfully!</h3></div>");
        }

        Utility::redirect('create.php');


    }
}