<?php
namespace App\Gender;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Gender extends DB{
    public $id = "";
    public $name = "";
    public $gender = "";
    public function __construct()
    {
        parent::__construct();
        echo "<br>";
    }
    public function setData($data=NULL){
        if(array_key_exists("id",$data)){
            $this->id=$data["id"];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data["name"];
        }
        if(array_key_exists("gender",$data)){
            $this->gender=$data["gender"];
        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->name,$this->gender);
        $query="insert into gender(name,gender) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $result= $sth->execute($values);

        if($result){
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ gender: $this->gender ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }else{
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ gender: $this->gender ] <br> Data Has Not Been Inserted Successfully!</h3></div>");
        }

        Utility::redirect('create.php');


    }
}
