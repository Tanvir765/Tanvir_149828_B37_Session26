<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birthday</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css" type="text/css">
</head>
<body>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Add Your Birthday Date</h1>
                        <div class="row-fluid user-row">
                            <img src="../../../resource/images/birthday.png" class="img-responsive icon" alt="Conxole Admin"/>
                            <style>body{
                                    background-image:url("../../../resource/images/birthday_bag.jpg");
                                    background-repeat: repeat-x;
                                }</style>
                        </div>
                    </div>

                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-birthday" method="Post" action="store.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <label class="" for="name">Your Name</label>
                            <input class="form-control" placeholder="Enter Your Name" name="name" type="text">
                            <label class="" for="name">Your Birthday Date</label>
                            <input type="date" name="birthday" class="form-control" id="birthday"" >
                            </br>
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Submit">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>