<?php

require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;

$obj = new BookTitle();
$allData = $obj->index();


//Without OBJ:::::::::::::::::::::::::
echo"<table border='5px'>";
foreach ($allData as $oneData) {
    echo "<tr>";
    echo "<td>" . $oneData['id'] . "</td>";
    echo "<td>" . $oneData['book_title']. "</td>";
    echo "<td>" . $oneData['author_name']. "</td>";
    echo "</tr>";
}
echo"</table>";




//with OBJ::::::::::::::::::::::::::
/*echo"<table border='5px'>";
foreach ($allData as $oneData) {
    echo "<tr>";
    echo "<td>" . $oneData->id . "</td>";
    echo "<td>" . $oneData->book_title. "</td>";
    echo "<td>" . $oneData->author_name. "</td>";
    echo "</tr>";
}
echo"</table>";
*/
?>